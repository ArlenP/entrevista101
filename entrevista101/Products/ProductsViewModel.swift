//
//  ProductsViewModel.swift
//  entrevista101
//
//  Created by Arlen Peña on 06/07/23.
//

import Foundation
import SwiftUI
import Combine

@MainActor
class ProductsViewModel: ObservableObject{
    
    @Published var productos: ProductsModel? = nil
    
    func getProduct() async throws {
    let _: Productos? = nil
        let url =  URL(string: "https://dummyjson.com/products")
        if let urlN = url{
            let request = URLRequest(url: urlN)
            let (data, _) =  try await URLSession.shared.data(for: request)
            self.productos = try JSONDecoder().decode(ProductsModel.self, from: data)
            if let productos = self.productos {
                print("Productos \(productos.products.count)")
            } else {
                print("No se encontraron productos")
            }
        }else {
            print("Error en la URL")
        }
    }
    
   
    
    
    
}


struct RemoteImage: View {
    @ObservedObject private var imageLoader = ImageLoader()
    private let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    var body: some View {
        Image(uiImage: imageLoader.image!)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .onAppear {
                imageLoader.loadImage(from: url)
            }
    }
}

class ImageLoader: ObservableObject {
    @Published var image: UIImage?
    
    private var cancellable: AnyCancellable?
    
    func loadImage(from url: URL) {
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data) }
            .replaceError(with: nil)
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
    }
}
