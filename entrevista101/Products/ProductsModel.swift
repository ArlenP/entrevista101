//
//  ProductsModel.swift
//  entrevista101
//
//  Created by Arlen Peña on 06/07/23.
//

import Foundation


struct ProductsModel:Codable{
    var products : [Productos]
}
struct Productos: Codable, Identifiable{
    let id: Int?
    let title: String?
    let description: String?
    let thumbnail: String?
}
