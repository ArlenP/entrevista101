//
//  entrevista101App.swift
//  entrevista101
//
//  Created by Arlen Peña on 06/07/23.
//

import SwiftUI

@main
struct entrevista101App: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
