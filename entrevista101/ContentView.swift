//
//  ContentView.swift
//  entrevista101
//
//  Created by Arlen Peña on 06/07/23.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @StateObject  var viewModel =  ProductsViewModel()
    

    var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(viewModel.productos?.products ?? []) { product in
                        NavigationLink(destination: ProductDetailView(product: product)) {
                            HStack {
                                Text(product.title ?? "")
                                    .font(.subheadline)
                                    .foregroundColor(.mint)
                            }
                        }
                    }
                }
                .navigationTitle("Productos")
            }
        }.onAppear{
            Task {
                do {
                    try await viewModel.getProduct()
                } catch {
                    print("Error loading data: \(error.localizedDescription)")
                }
            }
        }
        
    }

   
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
