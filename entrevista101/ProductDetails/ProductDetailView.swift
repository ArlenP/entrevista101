//
//  ProductDetailView.swift
//  entrevista101
//
//  Created by Arlen Peña on 06/07/23.
//

import SwiftUI

struct ProductDetailView: View {
    let product: Productos
    
    var body: some View {
        VStack {
            Text(product.title ?? "")
                .font(.title)
            Text(product.description ?? "")
                .foregroundColor(.gray)
            Spacer()
            VStack {
                if let imageUrl = product.thumbnail {
                    AsyncImage(url: URL(string: imageUrl)) { image in
                        image
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .clipShape(Circle())
                            .overlay(
                                Circle()
                                    .stroke(Color.gray, lineWidth: 2)
                            )
                    } placeholder: {
                        Color.gray
                            .frame(maxWidth: 400, maxHeight: 400)
                    }
                }
            }
            .frame(maxWidth: 400, maxHeight: 400)
            .padding(10)
            Spacer()
        }
        .padding()
        .background(
            LinearGradient(
                gradient: Gradient(colors: [Color.mint.opacity(0.1), Color.white]),
                startPoint: .top,
                endPoint: .bottom
            )
        )
        //.navigationTitle(product.title ?? "")
    }
}



struct ProductDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let product = Productos(id: 1, title: "Product Title", description: "Product Description", thumbnail: "https://i.dummyjson.com/data/products/2/thumbnail.jpg")
        return ProductDetailView(product: product)
    }
}
